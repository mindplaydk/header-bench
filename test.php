<?php

use mindplay\benchpress\Benchmark;
use Nyholm\Psr7\Response;
use Nyholm\Psr7\Stream;
use Psr\Http\Message\ResponseInterface;

require __DIR__ . "/vendor/autoload.php";

function mock_response(int $num_headers): ResponseInterface
{
    $headers = [];

    for ($i = 0; $i < $num_headers; $i++) {
        $headers["X-" . sha1($i)] = sha1($i . "0");
    }

    $body = Stream::create(str_repeat("0", 1024 * 32));

    return new Response(200, $headers, $body);
}

$bench = new Benchmark();

foreach ([10,20,100] as $num_headers) {
    $response = mock_response($num_headers);

    $bench->add(
        "Copy Response with {$num_headers} headers via withAddedHeader()",
        function () use ($response) {
            $duplicate = new Response(200);

            $duplicate = $duplicate->withBody($response->getBody());

            foreach ($response->getHeaders() as $name => $value) {
                $duplicate = $duplicate->withAddedHeader($name, $value);
            }

//            $count = count($duplicate->getHeaders());
//
//            echo "{$count}\n";
        }
    );

    $bench->add(
        "Copy Response with {$num_headers} headers via constructor",
        function () use ($response) {
            $duplicate = new Response(200, $response->getHeaders());

            $duplicate = $duplicate->withBody($response->getBody());

//            $count = count($duplicate->getHeaders());
//
//            echo "{$count}\n";
        }
    );
}

$bench->run();
